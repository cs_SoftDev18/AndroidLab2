package com.example.a1437641.loan;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }


    @Override
    public void onClick(View v)
    {
        String loanAmount = ((EditText) findViewById(R.id.LoanAmount)).getText().toString();
        String numYears = ((EditText) findViewById(R.id.numYears)).getText().toString();
        String numRate = ((EditText) findViewById(R.id.numRate)).getText().toString();

        try {
            ((TextView) findViewById(R.id.errormsg)).setText("");

            double loan = Double.parseDouble(loanAmount);
            int years = Integer.parseInt(numYears);
            double rate = Double.parseDouble(numRate);

            if(rate > 100 || years < 1 || years > 25 || loan < 0 || years < 0 || rate < 0)
                throw new IllegalArgumentException("Invalid numbers were passed");

            LoanCal cal = new LoanCal(loan, years, rate);

            EditText resultMonth = ((EditText) findViewById(R.id.monthPay));
            EditText resultPlay = ((EditText) findViewById(R.id.totalPayment));
            EditText resultInt = ((EditText) findViewById(R.id.totalInt));

            resultMonth.setText(cal.getMonthlyPayment() + "");
            resultPlay.setText(cal.getTotalCostOfLoan() + "");
            resultInt.setText(cal.getTotalInterest() + "");
        }
        catch(IllegalArgumentException e)
        {
            ((TextView) findViewById(R.id.errormsg)).setText(e.getMessage());
        }

    }


    public void clearText(View v)
    {
        ((EditText) findViewById(R.id.LoanAmount)).setText("");
        ((EditText) findViewById(R.id.numYears)).setText("");
        ((EditText) findViewById(R.id.numRate)).setText("");

        ((EditText) findViewById(R.id.monthPay)).setText("");
        ((EditText) findViewById(R.id.totalPayment)).setText("");
        ((EditText) findViewById(R.id.totalInt)).setText("");

    }




}
